(local anim8 (require "lib.anim8"))
(local ui (require "ui"))
(local utils (require "utils"))

(local sprites {:slimes {} :chest {} :heros {}})

(local slime-sprite-sheet (love.graphics.newImage "assets/slimes.png"))

(local chest-sprite-sheet (love.graphics.newImage "assets/chest.png"))

(local hero-sprite-sheet (love.graphics.newImage "assets/temp-hero.png"))

(local slime-grid       
       (anim8.newGrid 64 64
                      (: slime-sprite-sheet :getWidth)
                      (: slime-sprite-sheet :getHeight)))

(local chest-grid       
       (anim8.newGrid 32 32
                      (: chest-sprite-sheet :getWidth)
                      (: chest-sprite-sheet :getHeight)))

(local hero-grid       
       (anim8.newGrid 32 32
                      (: hero-sprite-sheet :getWidth)
                      (: hero-sprite-sheet :getHeight)))



(fn rand-region [low high]
    (+ (* (math.random) (+ high (- low)))  low ))

(fn standard-speed []
    0.1)

(fn slow-speed []
    0.3)

(fn set-speed [x]
  x)

(fn rand-speed []
    (let [rs (rand-region 0.08 0.12)]
      rs))

(local slime-regions
       {:attack
        {:r [[[1 10 1]] standard-speed]
            :l [[[20 11 1]] standard-speed]
            :dr [[[1 10 2]] standard-speed]
            :dl [[[20 11 2]] standard-speed]}
        :move
        {:r [[[1 2 7]] slow-speed]
            :l [[[20 19 7]] slow-speed]
            :dr [[[1 2 8]] slow-speed]
            :dl [[[20 19 8]] slow-speed]}
        :dead
        {:r [[[10 10 7]] slow-speed]
            :l [[[14 14 7]] slow-speed]
            :dr [[[10 10 8]] slow-speed]
            :dl [[[14 14 8]] slow-speed]}
        :dying
        {:r [[[7 10 7]] slow-speed]
            :l [[[14 11 7]] slow-speed]
            :dr [[[7 10 8]] slow-speed]
            :dl [[[14 11 8]] slow-speed]}
        :idle        
        {:r [[[1 10 5]] rand-speed]
            :l [[[20 11 5]] rand-speed]
            :dr [[[1 10 6]] rand-speed]
            :dl [[[20 11 6]] rand-speed]}
       :split-1
       {:r [[[1 9 9]] standard-speed]
        :l [[[20 11 9]] standard-speed]
        :dr [[[1 9 10]] standard-speed]
        :dl[[[20 8 9][8 9 3]] standard-speed]}
        :split-2
       {:r [[[10 10 9]] standard-speed]
        :l [[[20 11 9]] standard-speed]
        :dr [[[1 10 10]] standard-speed]
        :dl[[[1 8 9][8 9 3]] standard-speed]}
        :rebirth
       {:r [[[1 3 3]] standard-speed]
        :l [[[20 17 3]] standard-speed]
        }
        }
        )

(local hero-regions
       {:attack
        {:r [[[1 5 5]] (partial set-speed 0.2)]
         :l [[[1 5 5]] (partial set-speed 0.2)]
         :dr [[[1 5 6]] (partial set-speed 0.2)]
         :dl [[[1 5 6]] (partial set-speed 0.2)]}
        :idle
        {:r [[[1 1 1]] slow-speed]
         :l [[[1 1 1]] slow-speed]
         :dr [[[1 1 2]] slow-speed]
         :dl [[[1 1 2]] slow-speed]}
        :move
        {:r [[[2 5 1]] (partial set-speed 0.2)]
            :l [[[2 5 1]] (partial set-speed 0.2)]
            :dr [[[2 5 2]] (partial set-speed 0.2)]
            :dl [[[2 5 2]] (partial set-speed 0.2)]}
        })

(fn sprites.slimes.animate [camera state]
    (each [_i slime (ipairs (-> state.slimes
                                (camera.sort-y)
                                (camera.filter-viewable camera.x camera.y
                                                        camera.w camera.h)))]
          (let [[xp yp] (camera.floor (camera.shift slime))]   
            (when slime.anim
              (when (and slime.select state.render.select)
                (ui.select xp yp))
              (: slime.anim :draw slime-sprite-sheet xp yp)
              (when (and slime.text (< slime.text.current slime.text.chosen))
                (ui.speech-bubble slime.text.value xp yp))
              (when state.render.hp
                  (ui.hbar slime.hp xp yp))
              (when state.render.collide
                (ui.collider (+ xp slime.collider.off-x)
                             (+ yp slime.collider.off-y)
                             slime.collider.w
                             slime.collider.h)
                )))))

(fn sprites.slimes.anim [a dir id callback]
    (var vals {})
   (let [[data scale] (. slime-regions a dir)]
      (each [_i  v (ipairs data)]            
            (let [[start end y] v]              
              (table.insert vals (.. start "-" end))
              (table.insert vals y)))
      (let [animation (anim8.newAnimation
                       (slime-grid (unpack vals))
                        (scale) callback)]
        (set animation.id id)
        animation)))

(fn sprites.chest.anim []
    (anim8.newAnimation (chest-grid 1 1) 100))

(fn sprites.chest.animate [camera state]
    (local chest (. state :l1 :chest))
    (let [[xp yp] (camera.floor (camera.shift chest))]
     (: chest.anim :draw chest-sprite-sheet xp yp 0 3 3)))

(fn sprites.heros.anim [a dir id callback]
  (var vals {})
  (let [[data scale] (. hero-regions a dir)]
      (each [_i  v (ipairs data)]            
            (let [[start end y] v]              
              (table.insert vals (.. start "-" end))
              (table.insert vals y)))
      (let [animation (anim8.newAnimation
                       (hero-grid (unpack vals))
                        (scale) callback)]
        (set animation.id id)
        animation)))

(fn sprites.heros.animate [camera state]
    (each [_i hero (ipairs (-> state.l1.heros
                                (camera.sort-y)
                                (camera.filter-viewable camera.x camera.y
                                                        camera.w camera.h)))]
          (let [[xp yp] (camera.floor (camera.shift hero))]   
            (when hero.anim
              (: hero.anim :draw hero-sprite-sheet xp yp 0 3 3)
              (when state.render.hp
                (ui.hbar hero.hp (+ xp 16) yp))
              (ui.ad hero.damage hero.defense xp yp)
              (when hero.text
                (ui.speech-bubble hero.text.value (+ xp 16) (- yp 10)))
              (when state.render.collide
                (ui.collider (+ xp hero.collider.off-x)
                             (+ yp hero.collider.off-y)
                             hero.collider.w
                             hero.collider.h))
              (when state.render.agro
                (local center (camera.floor (camera.shift (utils.center hero))))
                (ui.agrorad (. center 1) (. center 2) 120))
              ))))

sprites
