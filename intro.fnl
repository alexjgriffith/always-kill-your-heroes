(local small-font (love.graphics.newFont "assets/Mali-Regular.ttf" 16))
(local subtitle-font (love.graphics.newFont "assets/Mali-Regular.ttf" 40))

(fn keypressed [key set-mode]
    (if (or (= "escape" key) (= "f11" key))
        (set-mode :menu)
        (set-mode :play-state)))

(fn draw-credits [credits y]
    (love.graphics.setFont small-font)
    (var first true)
    (var pos y)
    (local step 30)
    (each [_i cred (ipairs credits)]
          (if first
              (do
               (set first false)
               (love.graphics.printf cred 0 pos 360 "center"))
              (do
               (set first true)
               (love.graphics.printf cred 360 pos 360  "center")
                (set pos (+ pos step))))))
    
    
(fn draw []
  (local introduction "After attending a four year collage program, you, the proto slime, are broke, and the sharks are knocking at the door. Clearly taking blob studies did not yield a tangible career. As a last ditch effort, before the debt collectors come and pop your bubble, you've put all of your coin into a treasure chest in hopes of luring in treasure hunters.\n\nYou are not alone however. You have your trusty progeny, just press R and see. Together you can make up the 20,000 necessary to pay off the sharks.\n\nBeware, the more treasure is in the chest the more mighty the treasure hunter. You might bite off more than you can chew. But on the other hand, no need to fear the debt collectors if your already dead.")
  
  
  (love.graphics.setFont subtitle-font)
  (love.graphics.printf "The Deadly Default" 0 32 720 "center")
  (love.graphics.setFont small-font)
  (love.graphics.printf "(Press SPC to continue)" 0 80 720 "center")
  
  (love.graphics.printf introduction 20 120 680 "left")
  )
(var counter 0)

(fn update [dt mouse set-state]
  (set counter (+ counter dt))
  (when (and (> counter 0.5)(love.mouse.isDown 1))
    (set-state :play-state)))

{:draw draw :update update
       :keypressed keypressed}
