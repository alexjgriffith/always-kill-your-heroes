(lambda reset [?state]
  (let [default
        {:slimes {} :coins 100 :selection {} :game-over{:coins 0 :dead false}
         :kill-list {} :message "(Press ESC to return to menu)"
         :damage-rate (* 1 (/ 1000 60)) :regen-rate (* 0.3 (/ 1000 60))
         :render {:collide false :hp true :select true :agro false}
         :l1 {:first true :world nil :chest {} :heros {}}
         :reset reset}]
    (when state        
        (each [i v (pairs default)]
          (tset ?state i v))
        )
    default))

(reset)
