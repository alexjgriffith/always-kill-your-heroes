(local slime {})

(local sprites (require "sprites"))
(local utils (require "utils"))
(local bump (require "lib.bump"))

(local slime-attack-text (lume.split (love.filesystem.read "text/slime-attack") "\n"))
(local slime-idle-text (lume.split (love.filesystem.read "text/slime-idle") "\n"))

(local slime-timer {:current 0 :chosen 5})

(set slime.speed (* 12 (/ 1000 60)))

(fn rand-region [low high]
    (+ (* (math.random) (+ high (- low)))  low ))

(fn slime.update [dt]
  (set slime-timer.current (+ dt slime-timer.current))
  (when (> slime-timer.current slime-timer.chosen)
    ;; choose a random slime that is either idle moving or attacking
    (local filtered (lume.filter state.slimes (fn [slime] (or (= slime.state :attack)
                                                              (= slime.state :move)
                                                              (= slime.state :idle)))))
    (when (> (# filtered) 0)
        (let [sl (. filtered (math.ceil (rand-region 0 (# filtered))))]
      (slime.set-text sl.id (if (= sl.state :attack)
                                (. slime-attack-text (math.floor (rand-region 1 (# slime-attack-text))))
                                (. slime-idle-text (math.floor (rand-region 1 (# slime-idle-text))))
                                ))))
    ;; generate a random time between 5 and 10 s for the next
    (tset slime-timer :chosen (math.floor (rand-region 5 10)))
    (tset slime-timer :current 0))
  (each [_i sl (ipairs state.slimes)]
    (when (~= sl.state :dead)
      (slime.update-position sl dt)
      (slime.update-text sl dt)
      (when sl.anim
        (: sl.anim :update dt)))))

(fn slime.update-text [sl dt]
  (when sl.text
    (set sl.text.current (+ dt sl.text.current))
    (when (> sl.text.current sl.text.chosen)
      (set sl.text false))))

(fn slime.deselect [id]
    (tset (. state.slimes id) :selected false)
    (set state.selection (lume.filter state.selection (fn [x] (~= x id)))))

(fn slime.reborn [a count]
  ;; start split-2 for current-slime
  (slime.next-state a count)  
  ;; start reborn for new slime
  (local sl (. state.slimes a.id))
  (slime.set-state sl.child :rebirth)
  (tset sl :child false))

(fn slime.remove [a count]
    (local sl (. state.slimes a.id))
    (tset sl :state :dead)
    ;; remove from selected
    ;; move hitbox to -80 -80
    ;; set x & y to -80 -80
    (slime.deselect a.id)
    (: state.l1.world :update a.id -80 -80)
    (tset sl :x -80)
    (tset sl :y -80)
    (tset sl :anim false))

(fn slime.next-state [a count]
    (local id a.id)
    (local sl (. state.slimes id))
    ;; idle move attack split dying dead (rebirth)
    (if
     (= sl.state :dying)
     (tset sl :anim (sprites.slimes.anim :dying sl.direction id slime.remove))
     (= sl.state :dead)
     (tset sl :anim false)
     (= sl.state :split-1)
     (if (= count 0)
         (tset sl :anim (sprites.slimes.anim :split-1 sl.direction id slime.reborn))
         (do
           (tset sl :state :idle)
           (tset sl :anim (sprites.slimes.anim :idle sl.direction id slime.next-state))))
     (= sl.state :split-2)
     (if (= count 0)
         (tset sl :anim (sprites.slimes.anim :split-2 sl.direction id slime.next-state))
         (do
           (tset sl :state :idle)
           (tset sl :anim (sprites.slimes.anim :idle sl.direction id slime.next-state))))
     (= sl.state :rebirth)
     (if (= count 0)
         (tset sl :anim (sprites.slimes.anim :rebirth sl.direction id slime.next-state))
         (do
           (tset sl :state :idle)
           (tset sl :anim (sprites.slimes.anim :idle sl.direction id slime.next-state))))
     (= sl.state :move)
     (tset sl :anim (sprites.slimes.anim :move sl.direction id slime.next-state))
     (= sl.state :attack)
     (tset sl  :anim (sprites.slimes.anim :attack sl.direction id slime.next-state))
     (do (tset sl :state :idle)
         (tset sl :anim (sprites.slimes.anim :idle sl.direction id slime.next-state)))))

(fn slime.set-state [id st]
    (local sl (. state.slimes id))
    (tset sl :state st)
    (slime.next-state {:id id} 0))

(fn slime.kill [id]
  (slime.deselect id)
  (local sl (. state.slimes id))
  (when sl.child
    (slime.kill sl.child))
  (tset sl  :hp 0)
  (slime.set-state id :dying))

(fn slime.make [id slime-state x y]    
    (tset
     state.slimes
     id
     {:id id :state slime-state
      :selected false
      :text {:current 0 :chosen 3 :value ""}
      :child false
      :type :slime
      :nomove 0
      :hp 100
      :collider{:off-x 7
                :off-y 7
                :w 50
                :h 50
                }
      :direction :r
      :destination false
      :h 64 :w 64
      :x x :y y
      :anim (sprites.slimes.anim slime-state :r id
                                 slime.next-state)})
    
    (: state.l1.world :add id  x   y  50 50))

(fn slime.set-text [id text]
  (local sl (. state.slimes id))
  (tset sl :text {:current 0 :chosen 3 :value text}))

(fn slime.split [id]
  ;; check if their is space in the current direction
  (local sl (. state.slimes id))
  (local [items len] [(: state.l1.world :queryRect (+ sl.x 64) sl.y 64 64)])
  (local dead-slimes (lume.filter state.slimes (fn [slime] (= slime.state :dead))))
  (when (and (= len 0) (> (# dead-slimes) 0) (> sl.hp 80))
    ;; choose a dead slime
    (let [reborn-slime (. dead-slimes 1)]
      ;; move collider to sl.x +64 sl.y
      (: state.l1.world :update reborn-slime.id (+ sl.x 64) sl.y)
      (tset sl :direction :r)
      (tset reborn-slime :direction :r)
      (tset reborn-slime :state :idle)
      (tset reborn-slime :x (+ sl.x 64))
      (tset reborn-slime :y sl.y)
      (tset reborn-slime :anim false)
      (tset reborn-slime :hp 20)
      (tset sl :hp (+  sl.hp  -80))
      (tset sl :child reborn-slime.id)
      (slime.set-state id :split-1)
      )    
    )
  )

(fn slime.goto [id x-in y-in]
    (tset (. state.slimes id) :nomove 0)
    (tset (. state.slimes id) :state :move)
    (tset (. state.slimes id) :destination {:x (+ x-in -32) :y (+ y-in -32)}))

(fn col-fun [item other]
    'slide')

(fn abs-min [a b]
    (if (< (math.abs a) (math.abs b))
        a
        b))

(fn slime.update-position [sl dt]
    (when (and sl.destination (~= sl.state :move))
      (set sl.destination false))
    (when (and sl.destination (~= sl.state :dead))
      (local x (+ sl.destination.x (- sl.x)))
      (local y (+ sl.destination.y (- sl.y)))
      (local distance (math.sqrt (+ (math.pow x 2) (math.pow y 2))))
      (local theta (math.atan2 y x))
      (local dx (abs-min x (* (math.cos theta) (* slime.speed dt))))
      (local dy (abs-min y (* (math.sin theta) (* slime.speed dt))))
      (local [new-x new-y cols len] [(: state.l1.world :move sl.id (+ sl.x dx) (+ sl.y dy)
                                        col-fun)])
      (if  (or (> (math.abs(- sl.x new-x)) 0) (> (math.abs (- sl.y new-y)) 0))
           (when (~= sl.state :move)
             (slime.set-state sl.id :move))
          (when (= sl.state :move)
            (slime.set-state sl.id :idle)))
      
      (when (or (and (> len 0 ) (< distance 64)) (and (= sl.x new-x) (= sl.y new-y))
                (> sl.nomove 10))
        (when (= sl.state :move)
          (slime.set-state sl.id :idle))
        (tset sl :destination false))
      
      (if (< (math.abs (- sl.x new-x)) 0.1)
          (tset sl :nomove (+ sl.nomove 1))
          (do
           (slime.change-dir sl.id dx dy)
           (set sl.x new-x)
           (tset sl :nomove 0)))
      
      (if (< (math.abs (- sl.y new-y)) 0.1)      
          (tset sl :nomove (+ sl.nomove 1))
          (do
           (slime.change-dir sl.id dx dy)
           (set sl.y new-y)
           (tset sl :nomove 0)))))

;; (fn slime.move [dt set-mode]
;;     (let [movement [[(or
;;                       (love.keyboard.isDown "left")
;;                       (love.keyboard.isDown "a")) -1 0]
;;                     [(or
;;                       (love.keyboard.isDown "right")
;;                       (love.keyboard.isDown "d")) 1 0]
;;                      [(or (love.keyboard.isDown "up")
;;                           (love.keyboard.isDown "w")) 0 -1]
;;                     [(or (love.keyboard.isDown "down")
;;                          (love.keyboard.isDown "s")) 0 1]]]
;;       (let [down
;;             (lume.map
;;              (lume.filter movement (fn [x] (let [[p _x _y] x] p)))
;;               (fn [v] [(. v 2) (. v 3)]))]
;;         (var dx 0)
;;         (var dy 0)
;;         (var mag 0)
;;         (each  [_i v (ipairs down)]
;;                       (let [[x y] v]
;;                         (set dy (+ dy y))
;;                         (set dx (+ dx x))))
;;         (set mag (math.sqrt (+ (math.pow dx 2) (math.pow dy 2)) ))
;;         (slime.change-dir 1 dx dy)
;;         (if (or (~= dx 0) (~= dy 0))
;;             (when (= (. state.slimes 1 :moving) false)
;;               (tset (. state.slimes 1) :moving true)
;;               (slime.next-state {:id 1} 0))
;;             (when (= (. state.slimes 1 :moving) true)
;;               (tset (. state.slimes 1) :moving false)
;;              (slime.next-state {:id 1} 0)             
;;              ))
;;         (tset (. state.slimes 1) :x (+ (. state.slimes 1 :x) (* dx (* slime.speed dt))))
;;         (tset (. state.slimes 1) :y (+ (. state.slimes 1 :y) (* dy (* slime.speed dt)))))))

(fn slime.change-dir [id dx dy]
    (let [change (fn [id dir]
                   (when (~= (. state.slimes id :direction) dir)
                     (tset (. state.slimes id) :direction dir)
                     (slime.next-state {:id id} 0))
                   )]
      (if (and (< dx 0) (< dy 0))
          (change id :l)
          (and (< dx 0) (>= dy 0))
          (change id :l)
          (and (>= dx 0) (< dy 0))
          (change id :r)
          (and (>= dx 0) (>= dy 0))
          (change id :r))))

slime
