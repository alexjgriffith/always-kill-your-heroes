(local camera {:x 0 :y 0 :w 720 :h 450 :bound false :speed (* 30 (/ 1000 60))})

(fn camera.filter-viewable [table x y w h]
    (lume.filter table
                 (fn [e]
                     (and
                      (> e.x (+ x -128)) (> e.y (+ y -128))
                      (< e.x (+ x w 128)) (< e.y (+ y w 128))))))

(fn clamp [x y]
    (if camera.bound
        [(math.min (math.max camera.x-min x) camera.x-max),
         (math.min (math.max camera.y-min y) camera.y-max)]
        [x y]))


(fn camera.center [x y]
    (let [[x y] (clamp
                 (+ x 32 (- (/ camera.w 2)))
                  (+ y 32 (- (/ camera.h 2))))]
    (set camera.x x)
    (set camera.y y)))

(fn camera.sort-y [table]
    (lume.sort table (fn [a b] (< a.y b.y))))

(fn camera.shift [element]
    [(- element.x camera.x)
     (- element.y camera.y)])


(fn camera.move [dt]
    (let [movement [[(or
                      (love.keyboard.isDown "left")
                      (love.keyboard.isDown "a")) -1 0]
                    [(or
                      (love.keyboard.isDown "right")
                      (love.keyboard.isDown "d")) 1 0]
                     [(or (love.keyboard.isDown "up")
                          (love.keyboard.isDown "w")) 0 -1]
                    [(or (love.keyboard.isDown "down")
                         (love.keyboard.isDown "s")) 0 1]]]
      (let [down
            (lume.map
             (lume.filter movement (fn [x] (let [[p _x _y] x] p)))
              (fn [v] [(. v 2) (. v 3)]))]
        (var dx 0)
        (var dy 0)
        (each  [_i v (ipairs down)]
                      (let [[x y] v]
                        (set dy (+ dy y))
                        (set dx (+ dx x))))

        (local mag (math.sqrt (+ (math.pow dx 2) (math.pow dy 2))))
        (when (or (~= dx 0 ) (~= dy 0))
          (let [[x y] (clamp
                       (+ (* (/ dx mag) (* camera.speed dt)) camera.x)
                        (+ (* (/ dy mag) (* camera.speed dt)) camera.y))]
            (set camera.x x)
            (set camera.y y))))))

(fn camera.floor [element]
    [(math.floor (. element 1)) (math.floor (. element 2))])

camera
