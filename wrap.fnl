(local repl (require "lib.stdio"))
(local canvas (love.graphics.newCanvas 720 450))
(local small-font (love.graphics.newFont "assets/Mali-Regular.ttf" 16))
(local sound (require "sound"))

(global state (require "state"))

(math.randomseed (os.time))

(local version "0.2.1")
(var scale 1.0)

;; set the first mode

(var mouse {:down false :triggered false  :x 0 :y 0 :x-down 0 :y-down 0})

(var mode nil)
(fn set-mode [mode-name ...]
  (set mode (require mode-name))
  (when mode.activate
    (mode.activate ...)))

 (set-mode "menu")

(fn love.load []
    (: canvas :setFilter "nearest" "nearest")
    (repl.start)
    (sound.play :bgm))

(fn love.draw []
    (love.graphics.setCanvas canvas)
    (love.graphics.clear)    
    (love.graphics.setColor 1 1 1)    
    (mode.draw)
    (love.graphics.setCanvas)
    (love.graphics.setColor 1 1 1)
    (love.graphics.setFont small-font)
    ;; (love.graphics.printf (.."WIP " version) 0 0 720 "left")
    (love.graphics.draw canvas 0 0 0 scale scale))

(fn love.update[dt]
    (mode.update dt mouse set-mode))

(fn love.mousereleased [x y button]
    (local camera (require "camera"))
    (tset mouse :triggered true)
    (tset mouse :down false)
    (tset mouse :x (+ x camera.x))
    (tset mouse :y (+ y camera.y))
    (when mode.mousereleased
      (mode.mousereleased mouse set-mode))
    ;;(print "Mouse released" button "at" x " " y)
    )

(fn love.mousemoved [x y]
    (local camera (require "camera"))
    (tset mouse :x (+ x camera.x))
    (tset mouse :y (+ y camera.y))
)

(fn love.mousepressed [x y button]
    (local camera (require "camera"))
    (tset mouse :triggered true)
    (tset mouse :down true)
    (tset mouse :x-down (+ x camera.x))
    (tset mouse :y-down (+ y camera.y))
    (tset mouse :x (+ x camera.x))
    (tset mouse :y (+ y camera.y))
    ;;(print "Mouse pressed" button "at" x " " y)
    )

(fn game-over-coin [set-mode]
    (pp "game over coin")
    (tset state.game-over :coins 0)
    (tset state.game-over :dead false)
    (set-mode :game-over))

(fn game-over-dead [set-mode]
    (pp "game over dead")
    (tset state.game-over :coins 100)
    (tset state.game-over :dead true)
    (set-mode :game-over))

(fn victory [set-mode]
    (pp "game over victory")
    (tset state.game-over :coins 21000)
    (tset state.game-over :dead false)
    (set-mode :game-over))

(fn love.keypressed [key]
    (if
     (and (love.keyboard.isDown "lctrl" "rctrl" "capslock") (= key "q"))
     (love.event.quit)
     ;; (= "f5" key ) (victory set-mode)
     ;; (= "f6" key) (game-over-dead set-mode)
     ;; (= "f7" key) (game-over-coin set-mode)
     ;; add what each keypress should do in each mode
     (mode.keypressed key set-mode)))
    
