(local sound (require "sound"))

(local small-font (love.graphics.newFont "assets/Mali-Regular.ttf" 16))
(local menu-font (love.graphics.newFont "assets/Mali-Regular.ttf" 32))
(local subtitle-font (love.graphics.newFont "assets/Mali-Regular.ttf" 64))
(local title-font (love.graphics.newFont "assets/Mali-Regular.ttf" 80))

(local buttons [{:text :play :y 230 :h1 200 :h2 280
                       :highlight {:x 230 :y 255 :next "intro"}}
                {:text :instructions :y 300 :h1 290 :h2 350
                       :highlight {:x 180 :y 325 :next "instructions"}}
                {:text :credits :y 370 :h1 360 :h2 430
                       :highlight {:x 210 :y 395 :next "credits"}}])

(var highlight false)

(fn draw-menu []
    (love.graphics.setFont subtitle-font)
    (love.graphics.printf "Always Kill Your" 0 32 720 "center")
    (love.graphics.setFont title-font)
    (love.graphics.printf "HEROES" 0 100 720 "center")
    (love.graphics.setFont small-font)
    (love.graphics.printf "alexjgriffith" 20 420 650 "right")
    (love.graphics.printf "music by - CyberSDF" 20 420 700 "left")
    (love.graphics.setFont menu-font)
    (each [_i button (ipairs buttons)]
          (love.graphics.printf button.text 0 button.y 720 "center"))    
    (when highlight
      (love.graphics.rectangle "fill" highlight.x
                               (+ highlight.y -3) 80 6)
      (love.graphics.rectangle "fill" (+ 720 -80 (- highlight.x))
                               (+ highlight.y -3) 80 6)))

(fn update-menu [dt mouse set-mode]
    (let [my (love.mouse.getY) down (love.mouse.isDown 1)]
      (var match false)
      (each [_i button (ipairs buttons)]
            (when (and (> my button.h1) (< my button.h2))
                (when (or (not highlight) (~= button.highlight.y highlight.y))
                  (sound.play :click)
                  (set highlight button.highlight))
                (set match true)))
      (when (not match)
        (set highlight false))
      (when down
        (when highlight
          (sound.play :click)
          (set-mode highlight.next)))))

{:draw draw-menu
 :update update-menu
 :keypressed (fn keypressed [key set-mode]
                       (print "key pressed in menu"))}
