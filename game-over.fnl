(local credits (lume.split (love.filesystem.read "text/credits") "\n"))

(local small-font (love.graphics.newFont "assets/Mali-Regular.ttf" 16))
(local subtitle-font (love.graphics.newFont "assets/Mali-Regular.ttf" 40))
(local font (love.graphics.newFont "assets/Mali-Regular.ttf" 20))

(fn keypressed [key set-mode]
  (if (or (= "escape" key) (= "f11" key))
      (do
        (local reset state.reset)
        (lume.clear state)
        (reset state)
        (set-mode :menu)
        )
      (set-mode :kill-list)))



(fn draw-credits [credits y]
    (love.graphics.setFont small-font)
    (var first true)
    (var pos y)
    (local step 30)
    (each [_i cred (ipairs credits)]
          (if first
              (do
               (set first false)
               (love.graphics.printf cred 0 pos 360 "center"))
              (do
               (set first true)
               (love.graphics.printf cred 360 pos 360  "center")
                (set pos (+ pos step))))))
    
(fn victory []
    (love.graphics.setFont subtitle-font)
    (love.graphics.printf "Winner" 0 32 720 "center")
    (love.graphics.setFont small-font)
    (love.graphics.printf "(Press ESC to return to quit)" 0 75 720 "center")

    (draw-credits credits 300))

(fn defeat []
  (var heading "")
  (var message "")           
  (if
   (= state.game-over.dead true)
   (do
     (set heading "You're Dead - Game Over!")
     (set message (.. "When you died you had " state.game-over.coins " coins in your chest.\n\nWas it really worth it?")))   
   (= state.game-over.coins 0)
   (do
     (set heading "You're Broke - Game Over!")
     (set message "You can hear the sharks knocking on the door. With no coin left you have no way out, this is the end."))
   (> state.game-over.coins 20000)
   (do
     (set heading "Victory!")
     (set message "You made it! So many heroes dead, but at least you can rest easy now and start a graduate program in blob studies. After all, what is the worst that could happen?")))
  
  (love.graphics.setFont subtitle-font)
  (love.graphics.printf  heading 0 32 720 "center")
  (love.graphics.setFont small-font)
  (love.graphics.printf "(Press SPC for your kill list or ESC to return to the menu)" 0 80 720 "center")  
  (love.graphics.printf message 20 120 680 "center")
  (love.graphics.setFont font)  
  (love.graphics.printf "Credits" 0 220 720 "center")
  (draw-credits credits 250))

    
(fn draw []    
    (if state.game-over-case
        (vicotry)
        (defeat)))

(fn update [dt mouse set-state])

{:draw draw :update update
       :keypressed keypressed}
