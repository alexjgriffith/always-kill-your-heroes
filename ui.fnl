(local ui {})
(fn ui.hbar [h x y]
    (love.graphics.setColor 1 0 0)    
    (love.graphics.rectangle "fill" (+ x 16)  y   (* (/ h 100) 32) 5)
    (love.graphics.setColor 1 1 1))

(fn ui.ad [attack defense x y]
    (love.graphics.printf (.. attack "/" defense) (+ x 32) (- y 20) 32 "center")
 )

(fn ui.speech-bubble [text x y ]
  (love.graphics.printf text (- x 224) (- y 30) 512 "center"))

(fn ui.select [x y]
    (love.graphics.setColor 1 1 0)
    (love.graphics.circle "fill" (+ 30 x) (+ 45 y) 25 7)
    (love.graphics.setColor 1 1 1))

(fn ui.agrorad [x y r]
  (love.graphics.setColor 1 1 0)
  (love.graphics.circle "line" x y r 200)
  (love.graphics.setColor 1 1 1))

(fn ui.collider [x y h w]
  (love.graphics.setColor 1 0 0)    
  (love.graphics.rectangle "line" x  y h w)
  (love.graphics.setColor 1 1 1))
ui
