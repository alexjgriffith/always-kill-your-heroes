(local instructions (lume.split (love.filesystem.read "text/instructions") "\n"))

(local small-font (love.graphics.newFont "assets/Mali-Regular.ttf" 16))
(local subtitle-font (love.graphics.newFont "assets/Mali-Regular.ttf" 40))

(fn keypressed [key set-mode]
    (if (or (= "escape" key) (= "f11" key))
        (set-mode :menu)))

(fn draw-instructions [instructions y]
    (love.graphics.setFont small-font)
    (var first true)
    (var pos y)
    (local step 30)
    (each [_i cred (ipairs instructions)]
          (if first
              (do
               (set first false)
               (love.graphics.printf cred 0 pos 360 "center"))
              (do
               (set first true)
               (love.graphics.printf cred 360 pos 360  "center")
                (set pos (+ pos step))))))
    
    
(fn draw []
    (love.graphics.setFont subtitle-font)
    (love.graphics.printf "Instructions" 0 32 720 "center")
    (love.graphics.setFont small-font)
    (love.graphics.printf "(Press ESC to return to menu)" 0 75 720 "center")
    (draw-instructions instructions 120))

(fn update [dt mouse set-state])

{:draw draw :update update
       :keypressed keypressed}
