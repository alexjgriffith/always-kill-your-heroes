(local sounds {:bgm {:type :music :source (love.audio.newSource "assets/idle.mp3" "stream")}
               :click {:type :sfx :source (love.audio.newSource "assets/click.wav" "static")}})

(local sound-levels {:music 0.3 :sfx 0.8})

(: sounds.bgm.source :setLooping true)

(: sounds.click.source :setPitch 0.5)


(fn set-sound-levels []
    (each [key value (pairs sound-levels)]
          (lume.map sounds
                    (fn [x]
                        (when (= x.type key)
                          (: x.source :setVolume value))))))

(set-sound-levels)

{:play (fn play [name]
           (when (and (not (: (. sounds name :source) :isPlaying))
                      (not (love.filesystem.getInfo "mute")))
             (: (. sounds name :source) :play)))
 :stop (fn [name] (: (. sounds name :source) :stop))}
