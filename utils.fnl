(local utils {})

(fn utils.center [s]
  {:x (+ s.x (/ s.w 2))
   :y (+ s.y (/ s.h 2))})

(fn utils.euclid [a b]
  (local ca (utils.center a))
  (local cb (utils.center b))
  (math.sqrt (+ (math.pow (+ cb.x (- ca.x)) 2)
                (math.pow (+ cb.y (- ca.y)) 2))))

utils
