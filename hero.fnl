(local hero {})

(local sprites (require "sprites"))
(local slime (require "slime"))
(local utils (require "utils"))

(local names (lume.split (love.filesystem.read "text/names") "\n"))
(local profs (lume.split (love.filesystem.read "text/profs") "\n"))
(local descs (lume.split (love.filesystem.read "text/descs") "\n"))


(local hero-attack-text (lume.split (love.filesystem.read "text/hero-attack") "\n"))

(set hero.speed (* 3 (/ 1000 60)))

(fn rand-region [low high]
    (+ (* (math.random) (+ high (- low)))  low ))

(fn hero.generate-name []
  (local name (. names (math.floor (rand-region 1 (# names)))))
  (local prof (. profs (math.floor (rand-region 1 (# profs)))))
  (local desc (. descs (math.floor (rand-region 1 (# descs)))))
  (.. desc  " " name " the " prof))

(global gn hero.generate-name)

(fn hero.quip []
  (local h (. state.l1.heros 1))
  (hero.set-text h.id (. hero-attack-text (math.floor (rand-region 1 (# hero-attack-text))))))

(fn hero.agroslimes [h]
  (local center (utils.center h))
  (var within-range {})
  (each [i s (pairs state.slimes)]
    ;;(pp (utils.euclid h s))
    (when (and (> h.agrorad (utils.euclid h s))
               (~= s.state :dead)
               (~= s.state :dying))
      (table.insert within-range s)))
  (if (> (# within-range) 0)
      ;; set state to attack
      (do (when (~= h.state :attack)
            (hero.quip)
            (hero.set-state 1 :attack))
          (tset h :within-range within-range)
          (each [i s (pairs within-range)]
            (when (and (~= s.state :attack) (~= s.state :move))
              (slime.set-state s.id :attack))))
      ;; set state to move
      (do (when (~= h.state :move)
            (hero.set-state 1 :move))
          (tset h :within-range {})
          (each [i s (pairs state.slimes)]
            (when (= s.state :attack)
              (slime.set-state s.id :idle))))))

(fn hero.update [dt]  
  (let [h (. state.l1.heros 1)]
    (hero.update-text h dt)
    (if (= h.state :dead)
        (do (set h.timer.current (+ h.timer.current dt))
            (when (> h.timer.current h.timer.chosen)
              (set h.timer.chosen (rand-region h.timer.min h.timer.max))
              (set h.timer.current 0)
              (hero.awaken h)))
      (do (hero.agroslimes h)
          (hero.update-position h dt)
          (when h.anim
            (: h.anim :update dt))))))

(fn hero.update-text [h dt]
  (when h.text
    (set h.text.current (+ dt h.text.current))
    (when (> h.text.current h.text.chosen)
      (set h.text false))))

(fn hero.next-state [a count]
  (local h (. state.l1.heros a.id))
  (if
   (= h.state :idle)
   (tset h :anim (sprites.heros.anim :idle h.direction h.id hero.next-state))
   (= h.state :move)
   (tset h :anim (sprites.heros.anim :move h.direction h.id hero.next-state))
   (= h.state :attack)
   (tset h :anim (sprites.heros.anim :attack h.direction h.id hero.next-state))
   (do
     (set h.state :dead)
     (tset h :anim false))))

(fn hero.set-state [id st]
    (local h (. state.l1.heros id))
    (tset h :state st)
    (hero.next-state {:id id} 0))

(fn hero.kill [id]  
  (local h (. state.l1.heros id))
  (tset state :message (.. h.name " has been vanquished!"))
  ;; set the hero state to dead  
  (hero.set-state id :dead)
  ;; add coins to coffers
  (set state.coins (+ state.coins h.coins))
  (table.insert state.kill-list  h.name)
  (pp state.kill-list)
  (tset h :name (hero.generate-name))
  ;; move hero off screen and reset
  (: state.l1.world :update h.bump-table  595 -100)
  (tset h :x 595)
  (tset h :y -100)
  (tset h :hp 100)
  ;; set all attacking slimes to idle
  (each [_i sl (pairs h.within-range)]
    (slime.set-state sl.id :idle))
  (tset h :within-range {})    
  (tset slime :anim false)  
  )

(fn hero.awaken [h]
  (tset state :message (.. h.name " has come for your treasure!"))
  (hero.set-state h.id :idle)
  ;; the hero's coins will be set to the value of your chest modified by the number of slimes alive
  (local alive (# (lume.filter state.slimes (fn [slime] (~= slime.state :dead)))))
  (local defense (math.floor (math.max 0 (rand-region (- alive 3) (- alive 1)))))
  (local damage (if (< state.coins 200)
                    1
                    (< state.coins 500)
                    2
                    (< state.coins 1000)
                    3
                    (< state.coins 5000)
                    4
                    (< state.coins 10000)
                    5
                    (< state.coins 100000)
                    (math.floor (rand-region 6 7))
                    (math.floor (rand-region 8 10))
                    ))
  (local coins (math.floor (* state.coins (/ alive 3))))
  (tset h :coins coins)
  (tset h :damage damage)
  (tset h :defense defense)
  (tset h :direction :r)
  (tset h :destination {:x 595 :y 1000 :result hero.return})  
  )

(fn hero.return []
  (local h (. state.l1.heros 1)) 
  (tset h :coins (+ h.coins (math.min h.coins state.coins)))
  (tset h :direction :dl )
  (tset state :coins (math.max 0 (+ state.coins (- h.coins))))
  (tset h :destination {:x 595 :y -100
                        :result (fn []
                                  (local h (. state.l1.heros 1))
                                  (tset h :coins 0)
                                  (hero.kill 1)
                                  (tset h :destination false)
                                  (tset h :direction :r)
                                  )})
  )

(fn hero.make [name coins damage defense x y]  
  (tset state.l1.heros
        1
        {:id 1
         :coins coins
         :state :dead
         :name name
         :timer {:current 0 :min 5 :max 10 :chosen 5}
         :direction :r
         :type :hero
         :hp 100
         :text {:current 0 :chosen 3 :value ""}
         :damage damage
         :defense defense
         :agrorad 120
         :within-range {}
         :x x
         :y y
         :h 96
         :w 96
         :bump-table {:type :hero :id 1}
         :destination {:x 595 :y 1000 :result hero.return}
         :collider{:off-x 16
                   :off-y 0
                   :w 64
                   :h 64
                   }                  
         :anim (sprites.heros.anim :idle :r 1 hero.next-state)})
  (local h (. state.l1.heros 1))
  (: state.l1.world :add h.bump-table (+ x h.collider.off-x)
     (+ y h.collider.off-y) h.collider.w h.collider.h)
  )

(fn hero.set-text [id text]
  (local h (. state.l1.heros id))
  (tset h :text {:current 0 :chosen 3 :value text}))

(fn col-fun [item other]
    'touch')

(fn abs-min [a b]
    (if (< (math.abs a) (math.abs b))
        a
        b))

(fn hero.update-position [h dt]
    (when (and h.destination (~= h.state :dead))
      (local x (+ h.destination.x (- h.x)))
      (local y (+ h.destination.y (- h.y)))
      (local theta (math.atan2 y x))
      (local dx (abs-min x (* (math.cos theta) (* hero.speed dt))))
      (local dy (abs-min y (* (math.sin theta) (* hero.speed dt))))
      (local [new-x new-y cols len] [(: state.l1.world :move h.bump-table (+ h.x dx) (+ h.y dy)
                                        col-fun)])
      ;; (pp cols)
      (if  (or (> (math.abs(- h.x new-x)) 0) (> (math.abs (- h.y new-y)) 0))
           (when (and (~= h.state :move) (~= h.state :attack))
             (hero.set-state h.id :move))
           (when (= h.state :move)
             (hero.set-state h.id :idle)))
      
      (if (and (< (math.abs (- new-x h.destination.x)) 20) (< (math.abs (- new-y h.destination.y)) 20))
          (h.destination.result)
          (do (set h.x new-x)
              (set h.y new-y)))))

hero
