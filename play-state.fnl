(local slime (require "slime"))
(local sprites (require "sprites"))
(local camera (require "camera"))
(local bump (require "lib.bump"))
(local sound (require "sound"))
(local tiled (require "lib.tiled"))
(local hero (require "hero"))

(local small-font (love.graphics.newFont "assets/Mali-Regular.ttf" 16))

(fn make-chest [x y]    
    (set state.l1.chest {:x x :y y :type :chest :hp 100
                            :anim (sprites.chest.anim) :id 6})
    (: state.l1.world :add state.l1.chest (+ x 32) (+ y 80) 40 10))

;;(make-chest 595 1000)

(fn activate []    
    (when state.l1.first
      (set camera.bound true)
      (set camera.x-max (+ (* 40 32) -720))
      (set camera.y-max (+ (* 40 32) -450))
      (set camera.y-min 0)
      (set camera.x-min 0)
      (camera.center 595 900)
      (set state.l1.world (bump.newWorld 512))
      (set state.l1.map (tiled "l1.lua" ["bump"]))
      ;; (: state.l1.map :bump_init state.l1.world)
      (hero.make "Lame Lary the lonley" 100 1 0 595 -100)
      (slime.make 1 :idle 550 870)      
      (slime.make 2 :idle 620 900)      
      (slime.make 3 :dead -80 -80)
      (slime.make 4 :dead -80 -80)
      (slime.make 5 :dead -80 -80)
      (make-chest 595 1000)
      (set state.l1.first false)))

(fn toggle-camera []
    ;; (if camera.bound
    ;;     (set camera.bound false)
    ;;     (set camera.bound true))
    (if state.render.collide
        (set state.render.collide false)
        (set state.render.collide true))
    (if state.render.agro
        (set state.render.agro false)
        (set state.render.agro true))
    (print "toggle camera lock"))

(fn rebirth []
    ;;(pp "rebirth")
    (each  [_i v (pairs state.selection)]
           ;;(slime.set-state v :split)
           (slime.split v)
           ))

(fn suicide []
    ;;(pp "suicide")
    (each  [_i v (pairs state.selection)]
           (slime.kill v)))

(fn attack []
    (each  [_i v (pairs state.selection)]
           (slime.set-state v :attack)))

(fn jump-to-slime [id]
    (local sl (. state.slimes id))
    (when (~= sl.state :dead)
      (camera.center sl.x sl.y)))

(local keymap {:t toggle-camera
                  ;; :space attack
                  ;; :1 (partial jump-to-slime 1)
                  ;; :2 (partial jump-to-slime 2)
                  ;; :3 (partial jump-to-slime 3)
                  ;; :4 (partial jump-to-slime 4)
                  ;; :5 (partial jump-to-slime 5)                  
                  :r rebirth
                  :k suicide})

(fn slime-mouse [items len mouse]

    (when (or (and (= len 0) (> (# state.selection) 0))
              (and (= len 1) (= (# state.selection) 1)
                   (= (. items 1) (. state.selection 1))))
      ;; (sound.play :click) ;add once implemented
        (each [_i id (ipairs state.selection)]
              (slime.goto id mouse.x mouse.y)))
    ;; Remove all currently selected if a new slime is clicked on      
    (when (> len 0)
      (sound.play :click)
      (set state.selection {})
      (each [_i slime (pairs state.slimes)]
            (set slime.select false)))
    ;; add new slime or slimes to the selection
    (when (= (type items) "table")        
      (each [_i v (ipairs items)]
            (table.insert state.selection v)
            (tset (. state :slimes v) :select true))))

(fn mousereleased [mouse set-mode]    
    (let [x (math.min mouse.x mouse.x-down)
            y (math.min mouse.y mouse.y-down)
            w (math.max 1 (math.abs (+ mouse.x (- mouse.x-down))))
            h (math.max 1 (math.abs (+ mouse.y (- mouse.y-down))))]
      ;; add filter to ensure only slimes come out!
      (local [items len] [(: state.l1.world :queryRect x y w h
                             (fn [item] (= (type item) "number")))])
      (slime-mouse items len mouse)))
      
    
(fn count []
    (# (lume.filter state.slimes (fn [slime] (~= slime.state :dead)))))

(fn render-coins []
    (var str "Coins: ")
    (if (< state.coins 10)
        (set str (.. str "00" state.coins))
        (and (> state.coins 9) (< state.coins 100))
        (set str (.. str "0" state.coins))
        (set str (.. str  state.coins)))
    str)

(fn rand-1 [max]
    (math.ceil (* (math.random) max)))

(fn damage [dt]
    (local h (. state.l1.heros 1))
    (local l (# h.within-range))
    (when (> l 0)
        (local taken (* state.damage-rate dt (math.max 0 (+ l (- h.defense)))))
        (tset h :hp (math.max 0 (+ h.hp (- taken))))
        (for [i 1 h.damage 1]
             (let [sl (. h.within-range (rand-1 l))]
               (tset  sl :hp (math.max 0 (+ sl.hp (- (* state.damage-rate dt)))))))))

(fn regenerate [dt]
    (each [_i sl (pairs state.slimes)]
          (when (and (~= sl.state :attack) (~= sl.state :dying) (< sl.hp 100))
            (tset  sl :hp (math.min 100 (+ sl.hp  (* state.regen-rate dt)))))))

(fn test-death []
    ;; check if hero is dead
    (let [h (. state.l1.heros 1)]
      (when (= h.hp 0)
        (hero.kill 1)))
    ;; check to see which slimes have died
    (each [_i sl (pairs state.slimes)]
          (when (and (= sl.hp 0) (~= sl.state :dying) (~= sl.state :dead))
            (slime.kill sl.id))))

(fn keypressed [key set-mode]
    (let [f (. keymap key)] 
      (if (or (= "escape" key) (= "f11" key))
          (set-mode :menu)     
          (= (type f) "function")
          (f))))


{:mousereleased mousereleased
 :activate activate
 :keypressed keypressed
 :update (fn [dt mouse set-mode]
             ;;(: state.l1.map :update dt)
             (camera.move dt)
             (slime.update dt)
             (hero.update dt)
             (damage dt)
             (regenerate dt)
             (test-death)
             (tset state.game-over :coins state.coins)
             (tset state.game-over :dead (= (count) 0))
             (when (or (= (count) 0)
                       (< (+ state.coins (. state.l1.heros 1 :coins)) 1)
                       (> state.coins 20000))
               (set-mode :game-over))
             ) 
 :draw (fn []
           (: state.l1.map :draw (- camera.x) (- camera.y))           
           (love.graphics.printf (render-coins) 20 420 680 "right")
           (love.graphics.printf (.. "Slimes: "  (count) "/5") 20 420 680 "left")
           (sprites.heros.animate camera state)
           (sprites.slimes.animate camera state)
           (sprites.chest.animate camera state)
           (love.graphics.printf state.message 0 0 720 "center")
           )}
